package auto;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchBrowser {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver;

        // Use WebDriverManager to handle WebDriver binary management
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        // Add any desired options here

        // Instantiate ChromeDriver with ChromeOptions
        driver = new ChromeDriver(options);

        driver.get("http://164.52.217.13");
        driver.manage().window().maximize();
        driver.findElement(By.id("userName")).sendKeys("HIC_ADMIN");
        driver.findElement(By.id("userPassword")).sendKeys("Test@123");
        driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();

        WebDriverWait wait = new WebDriverWait(driver, 5); // 5 seconds timeout
        WebElement imgElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='/assets/icons/list2.svg']")));
        imgElement.click();

        WebDriverWait driver1 = new WebDriverWait(driver, 10);
        driver1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='btn black-button-cbt ng-star-inserted']"))).click();

        WebDriverWait driver2 = new WebDriverWait(driver, 10);
        driver2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='assets/reportloss/stage-icons/Insured Details.svg']")))
                .click();

        WebDriverWait driver3 = new WebDriverWait(driver, 10);
        driver3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//*[@type='text'])[1]"))).sendKeys("TN823982398923");

        WebDriverWait driver4 = new WebDriverWait(driver, 10);
        driver4.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@class, 'ng-star-inserted')]")))
                .click();

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        WebElement element = wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='mr-2']//*[contains(@class, 'ng-star-inserted')]")));

        element.click();

        // Close the browser
        driver.quit();
    }
}
