package pagefactory;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Mytest {
	
	WebDriver driver;
	Loginpage2 lp;
	
	@BeforeClass
	void setup() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		Thread.sleep(1000);
		driver.get("https://www.facebook.com/");
		
	}
	@Test
	void test() {
		lp.Setusername();
		lp.setPass();
		lp.login();
	}
	@AfterClass
	void clsoe() {
		driver.quit();
	}

}
