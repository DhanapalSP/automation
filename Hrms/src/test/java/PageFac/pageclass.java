package PageFac;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.Scenario;

public class pageclass {

	WebDriver ldriver;
	WebDriverWait wait;
//	String month = "Mar";
//	String year = "2024";

	public pageclass(WebDriver rdriver) {
		this.ldriver = rdriver;
		PageFactory.initElements(ldriver, this);
	}

	@FindBy(id = "username")
	WebElement username;
	@FindBy(id = "password")
	WebElement pwd;
	@FindBy(id = "loginsubmit")
	WebElement login;
	@FindBy(id = "dd")
	WebElement create;
	@FindBy(xpath = "(//a[normalize-space(text())='Leave Request'])[1]")
	WebElement leavereq;
	@FindBy(xpath = "//input[@value='Apply Leave']")
	WebElement apply;
	@FindBy(xpath = "//div/div[@id='s2id_leavetypeid']")
	WebElement selectdro;
	@FindBy(xpath = "//span[text()='Casual Leave']")
	WebElement casual;
	@FindBy(id = "reason")
	WebElement reason;
	@FindBy(xpath = "(//img[contains(@class, 'ui-datepicker-trigger')])[1]")
	WebElement fromdateTrigger;
	@FindBy(id="Canceldialog")
	WebElement can;

	@FindBy(xpath = "//a[text()='15']")
	WebElement fromDateLink;

	@FindBy(xpath = "(//img[@class='ui-datepicker-trigger'])[2]")
	WebElement toDateTrigger;

	@FindBy(xpath = "//a[text()='25']")
	WebElement toDateLink;

	public void setUsername(String uname) {
		username.sendKeys(uname);
	}

	public void setPass(String pwdd) {
		pwd.sendKeys(pwdd);
	}

	public void clicklogin() {
		login.click();
	}

	public void waitForCreateButton() throws InterruptedException {
		Thread.sleep(1000);
		create.click();
	}

	public void leavereqes() throws InterruptedException {

		Thread.sleep(1000);

		leavereq.click();
	}

	public void applyleave() {

		apply.click();
	}

	public void selectdrop() {
		selectdro.click();
		casual.click();
	}

	public void enterReasone(String description) {
		reason.sendKeys(description);

	}

	public void enterFromAndTo(String month, String year) throws InterruptedException {
		ldriver.findElement(By.id("from_date")).click();
		Thread.sleep(1000);
		// ldriver.findElement(By.xpath("//select[@data-handler='selectMonth']"));

		while (true) {
			String currentMonth = new Select(ldriver.findElement(By.xpath("//select[@data-handler='selectMonth']")))
					.getFirstSelectedOption().getText();

			String currentYear = new Select(ldriver.findElement(By.xpath("//select[@data-handler='selectYear']")))
					.getFirstSelectedOption().getText();

			if (currentMonth.equals(month) && currentYear.equals(year)) {
				break;
			} else {
				ldriver.findElement(By.xpath("//span[text()='Next']")).click();
				Thread.sleep(1500);// Small delay for next month to load
			}
		}

	
		WebElement clk = ldriver.findElement(By.xpath("//a[text()='20']")); // Assuming 15 is the date to select
		Thread.sleep(1000);
		clk.click();
	}

	public void entertodate(String month, String year) throws InterruptedException {
		ldriver.findElement(By.id("to_date")).click();
		Thread.sleep(1000);
		while (true) {
			String currentMonth = new Select(ldriver.findElement(By.xpath("//select[@data-handler='selectMonth']")))
					.getFirstSelectedOption().getText();

			String currentYear = new Select(ldriver.findElement(By.xpath("//select[@data-handler='selectYear']")))
					.getFirstSelectedOption().getText();
			if (currentMonth.equals(month) && currentYear.equals(year)) {
				break;
			} else {
				Thread.sleep(500);
				ldriver.findElement(By.xpath("//span[text()='Next']")).click();
				//Thread.sleep(500); // Small delay for next month to load
			}
		}

		Thread.sleep(500);
		ldriver.findElement(By.xpath("//a[text()='22']")).click(); // Assuming 15 is the date to select
	}
	public void cancelbutton() {
//	RemoteWebDriver pdriver;
//		boolean failed = scne.isFailed();
//		System.out.println(failed);
//		if(failed) {
//			byte[] screenshotAs = pdriver.getScreenshotAs(OutputType.BYTES);
//			
		//}
	can.click();
		
	}

}
