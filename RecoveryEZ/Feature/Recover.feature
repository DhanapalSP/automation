Feature: RecoverEZ Application

  Scenario Outline: Successfull login with valid credentials
    Given the user launch the broser "http://164.52.217.13/#/login/insurance-company"
    When user enteres as username "<username>" and password as "<password>"
    When user clicks on login button
    When user click the receivable button
    When User click the report loss button
    When User click the Insured details section
    And User Enter the Registered Numbers
    And Click to select the make as "AMMANN"
    And Click to select the model as "LG956L"
    When User click the date of purchase
    And user select the currencytype
    When user enter the Insured name
    And user click the save button
    When user click the tp section
    And select the TP company name
    When Enter the loss details section details
    And Click save button
    And click the police report section
    When User click the savee button
    When User click the garage details
    When User click the survey details
    And User click to save the survey details

    Examples: 
      | username  | password |
      | HIC_ADMIN | Test@123 |
      #|UIC_ADMIN|Password@123|
