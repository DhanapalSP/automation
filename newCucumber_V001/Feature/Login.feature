Feature: Login
Scenario: Successful login with valid credentials
    Given User Launch Edge browser
    When User opens URL "https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F"
    And User enters Email as "admin@yourstore.com" and password as "admin"
    And click on login 
    Then page title should be "Dashboard / nopCommerce administration"
    When user click on log out link
    And close browsers