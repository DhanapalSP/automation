package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Loginpage2 {

	//Constructor
	
	WebDriver driver;	
	
	Loginpage2(WebDriver driver){
		 this.driver=driver;
		 PageFactory.initElements(driver, this);
	 }
	
	//Identification
	
	@FindBy(id="email")WebElement username;
	@FindBy(id="pass")WebElement pwd;
	@FindBy(name="login")WebElement login;
	
	//Action
	
	public void Setusername() {
		username.sendKeys("dhanapal");
	}
	
	public void setPass() {
		pwd.sendKeys("Test@123");
	}
	public void login() {
		login.click();
	}
	
}
