package DataProvider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class DataProvider {

	WebDriver driver;

	@BeforeClass
	@Parameters({ "browser", "url" })

	void setup(String br, String url) throws InterruptedException {
		
		switch(br.toLowerCase()) {  //value defined in xml file like chrome,edge,firefox like that but here "tolowercase" method in will convert into coming value whatever it will convert in to lower case here.
		
		case "chrome" :driver=new ChromeDriver();break;
		case "edge"   :driver=new EdgeDriver();break;
		case "firefox":driver=new FirefoxDriver();break;
		default: System.out.println("Browser not matching...");
		return;
		}
		
//		if (br.equals("firefox")) {
//			Thread.sleep(1000);
//			WebDriverManager.firefoxdriver().setup();
//			driver = new FirefoxDriver();
//		} else if (br.equals("edge")) {
//			WebDriverManager.edgedriver().setup();
//			driver = new EdgeDriver();
//		} else {
//			WebDriverManager.firefoxdriver().setup();
//			driver = new FirefoxDriver();
//		}

		Thread.sleep(1000);
		driver.get(url);
		driver.manage().window().maximize();
		Thread.sleep(1000);
	}

	@Test(priority = 1)
	void testlogo() {
		try {
			boolean status = driver.findElement(By.xpath("//img[@alt='company-branding']")).isDisplayed();
			Assert.assertEquals(status, true);
		} catch (Exception e) {

			Assert.fail();
		}
	}

	@AfterClass
	void closeApp() {
		driver.quit();
	}

}
