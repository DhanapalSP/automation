package Runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features=".//Feature//Login.feature",
			glue="StepDef",
			monochrome = true,
			plugin= {"pretty", "html:test-output"}
				
			
			
			)
	public class testRun extends AbstractTestNGCucumberTests {

}
