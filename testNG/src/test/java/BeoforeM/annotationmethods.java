package BeoforeM;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class annotationmethods {
	
	@BeforeClass
	public void beforeclass() {
		System.out.println("Before class...");
	}
	@AfterClass
	public void afterclass() {
		System.out.println("After class...");
	}
	
	@Test
	void login() {
		System.out.println("Login successful");
	}
	
	@Test
	void homepage()
	{
		System.out.println("Landing homepage");
	}
	
	@Test
	void logout() {
		System.out.println("Logout successful");
	}
}
