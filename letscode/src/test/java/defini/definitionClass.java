package defini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/*import org.openqa.selenium.WebElement;*/
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class definitionClass {
	
	WebDriver driver;
	@Given("User launch the chrome browser and open the URL {string}")
	public void user_launch_the_chrome_browser_and_open_the_url(String url) {
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
	   driver.get(url);
	}

	@When("User enteres the email as {string} and password as {string}")
	public void user_enteres_the_email_as_and_password_as(String email, String pwd) throws InterruptedException {
	WebElement web =driver.findElement(By.xpath("//input[@value='admin@yourstore.com']"));
	web.clear();
	web.sendKeys(email);
	Thread.sleep(1000);
	  WebElement s = driver.findElement(By.xpath("//input[@value='admin']"));
	  s.clear();
	  s.sendKeys(pwd);
	   Thread.sleep(1000);
	   
	}

	@When("User clicks on the login button")
	public void user_clicks_on_the_login_button() {
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	}

	/*
	 * @When("User verify the successfull login") public void
	 * user_verify_the_successfull_login() {
	 * 
	 * 
	 * }
	 */
	@When("User clicks on logout button")
	public void user_clicks_on_logout_button() {
		driver.findElement(By.linkText("Logout")).click();	   
	}

}
