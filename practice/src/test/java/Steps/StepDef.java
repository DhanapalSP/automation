package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class StepDef {
	
	WebDriver driver;
	
	@Given("the user launch the chrome browser")
	public void the_user_launch_the_chrome_browser()  {
		System.out.println("Attempting to launch the browser...");
		driver = new ChromeDriver(); 
		System.out.println("Browser launched successfully!");
	}
	@When("user opens the URL {string}")
	public void user_opens_the_url(String url){
	     driver.get(url);
		 driver.manage().window().maximize();
		  
	}
	@When("User enters Email as {string} and password as {string}")
	public void user_enters_email_as_and_password_as(String email, String password) {
		driver.findElement(By.xpath("//input[@class='email valid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys(password);
	}
	@When("user click on login")
	public void user_click_on_login() {
		driver.findElement(By.xpath("//button[@class='button-1 login-button']")).click();
	    
	}
	@Then("User view the dashboard page")
	public void user_view_the_dashboard_page() {
	    
	}
	@Then("User click the customer menu")
	public void user_click_the_customer_menu() {
	   
	}
	@Then("click on customers menu item")
	public void click_on_customers_menu_item() {
	  
	}
	@Then("Click on Add new button")
	public void click_on_add_new_button() {
	   
	}
	@Then("User view the Add new customer page")
	public void user_view_the_add_new_customer_page() {
	    
	}
	@When("User enter customer info")
	public void user_enter_customer_info() {
	    
	}
	@When("Click on save button")
	public void click_on_save_button() {
	    
	}
	@Then("User can view confirmation message {string}")
	public void user_can_view_confirmation_message(String string) {
	   
	}
	@Then("Close the browser")
	public void close_the_browser() {
	
	
	
	}
	

}
