package pageobh;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
@Component
public class pageObj {
	
	@FindBy(xpath = "//input[@id='userName']")
	private static WebElement username;
	public WebElement getUsername() {
		return username;
	}
	@FindBy(xpath = "//input[@id='userPassword']")
	private static WebElement password;
	public WebElement getPassword() {
		return password;
	}
	@FindBy(xpath="//button[text()='Login']")
	private static WebElement login;
	public WebElement getLogin() {
		return login;
		
	}
	
	

}
