Feature: Customers
Scenario: Add new customer
Given the user launch the chrome browser
When user opens the URL "https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F"
And User enters Email as "admin@yourstore.com" and password as "admin"
And user click on login
Then User view the dashboard page
And User click the customer menu
And click on customers menu item
And Click on Add new button
Then User view the Add new customer page
When User enter customer info
And Click on save button
Then User can view confirmation message "The new customer has been added successfully."
And Close the browser