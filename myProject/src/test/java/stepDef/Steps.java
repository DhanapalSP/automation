package stepDef;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import PagrObject.LoginPage;

public class Steps {

    private WebDriver driver;
    private LoginPage lp;
    

    @Given("User launches the Chrome browser")
    public void user_launches_the_chrome_browser() {
    
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        lp = new LoginPage(driver);
    }

    @When("User opens the URL {string}")
    public void user_opens_the_url(String url) {
        driver.get(url);
    }

    @When("User enters the username as {string} and password as {string}")
    public void user_enters_the_username_as_and_password_as(String username, String password) {
        lp.enterUsername(username);
        lp.enterPassword(password);
    }

    @When("Clicks on Login")
    public void clicks_on_login() {
        lp.clickLogin();
    }
    
    
    @When("Clicks on receivable button")
    public void clicks_on_receivable_button() {
        lp.recebutton();
    }
   

    @When("User Clicks on receivable button")
    public void user_clicks_on_receivable_button() {
    	lp.reportloss();
    }

    @When("Clicks the insured details section button")
    public void clicks_the_insured_details_section_button() {
         lp.insured();
    }

    @When("Clicks the Registratiob number button and enter the values")
    public void clicks_the_registratiob_number_button_and_enter_the_values() {
       lp.regis();
    }

    @When("Enter the make dropdown fields and selecte the dropdown value")
    public void enter_the_make_dropdown_fields_and_selecte_the_dropdown_value() {
    	lp.make();
    }

    
    
}
