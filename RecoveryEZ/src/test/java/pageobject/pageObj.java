package pageobject;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

	public class pageObj{
		public static RemoteWebDriver driver;
		public static WebDriverWait wait;
		WebElement sel;
		
		@Given("the user launch the broser {string}")
		public void the_user_launch_the_broser(String url) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.get(url);
			driver.manage().window().maximize();

		}

		@When("user enteres as username {string} and password as {string}")
		public void user_enteres_as_username_and_password_as(String uname, String pwd) {
			WebElement web1 = driver.findElement(By.xpath("(//input[contains(@class,'form-control input-box')])[1]"));
			web1.clear();
			web1.sendKeys(uname);
			WebElement web2 = driver.findElement(By.xpath("(//input[contains(@class,'form-control input-box')])[2]"));
			web2.clear();
			web2.sendKeys(pwd);
		}

//		    @When("user clicks on login button")
//		    public void user_clicks_on_login_button()  {
//		   
//		    WebDriverWait wait = new WebDriverWait(driver, 15);
//			WebElement login = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Login')]")));
//			login.click();
//			wait.until(ExpectedConditions.visibilityOf(login));
//			wait.until(ExpectedConditions.invisibilityOf(login));
//			
//					
//		}

		@When("user clicks on login button")
		public void user_clicks_on_login_button() {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement login = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Login')]")));
			login.click();

			// Wait for the successful login message to appear
			WebElement successMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("toast-container")));

			// Perform actions after successful login message appears
			
						
			
			if (successMessage.isDisplayed()) {
				// Perform actions after successful login
				performAfterLoginActions();
			} else {
				// Handle if success message is not displayed
				System.out.println("Success message not displayed after login.");
			}
		}

		private void performAfterLoginActions() {
			// Perform actions after successful login
			System.out.println("Performing actions after successful login...");
			// Here you can add the actions you want to perform after successful login
		}

		@When("user click the receivable button")
		public void user_click_the_receivable_button() throws InterruptedException {

			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement receivableButton = wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//img[contains(@src, '/assets/icons/list2.svg')]")));
			receivableButton.click();

//			driver.findElement(By.xpath("(//span[@class='menu-container hover-text']//img)[1]/parent::span")).click();

		}

		@When("User click the report loss button")
		public void user_click_the_report_loss_button() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement reportLossButton = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[.='Report Loss']")));
			reportLossButton.click();

		}

		@When("User click the Insured details section")
		public void user_click_the_insured_details_section() throws InterruptedException {

			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement insured = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h4[.='Insured Details']")));
			insured.click();

		}

		@When("User Enter the Registered Numbers")
		public void user_enter_the_registered_numbers() throws InterruptedException {
			Thread.sleep(1000);
			driver.findElement(By.xpath("(//input[contains(@class,'form-control ng-untouched')])[1]"))
					.sendKeys("REG27932787893223");
		}

		@When("Click to select the make as {string}")
		public void click_to_select_the_make_as(String string) {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement ele1 = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select/option[@value='ARIA']")));
			ele1.click();

		}

		@When("Click to select the model as {string}")
		public void click_to_select_the_model_as(String string) {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement ele2 = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select/option[@value='ARIA PICK UP']")));
			ele2.click();

		}

		@When("User click the date of purchase")
		public void user_click_the_date_of_purchase() {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			WebElement ele;
			ele = wait.until(ExpectedConditions.presenceOfElementLocated(
					By.xpath("//span[contains(@class,'mat-mdc-button-touch-tar')]/ancestor::button")));
			ele.click();
			ele = driver.findElement(By.xpath("//span[text()=' 1 ']"));
			ele.click();
			ele = driver.findElement(By.xpath("//div[contains(@class,'cdk-overlay-container')]"));
			ele.click();

		}

		@When("user select the currencytype")
		public void user_select_the_currencytype() throws InterruptedException {

			WebDriverWait wait1 = new WebDriverWait(driver, 20);
			WebElement ele = wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
					"/html/body/app-root/app-report-loss-common/div/div/div/div/div/div/div/div/div/app-report-loss-stage-card/div/app-report-loss-stage-list/div/div[2]/div/div[5]/div/select")));
			Select dropdown = new Select(ele);
			dropdown.selectByIndex(1);

		}

		@When("user enter the Insured name")
		public void user_enter_the_insured_name() {
			WebDriverWait ele = new WebDriverWait(driver, 20);

			WebElement save = ele.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//label[text()=' Insured Name']/following-sibling::input")));
			save.sendKeys("paramesh");

		}

		@When("user click the save button")
		public void user_click_the_save_button() {

			WebDriverWait btn = new WebDriverWait(driver, 10);
			WebElement cl = btn.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()=' Save ']")));
			cl.click();

		}

		@When("user click the tp section")
		public void user_click_the_tp_section() {
			WebDriverWait vet = new WebDriverWait(driver, 20);
			WebElement clciElement = vet.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("(//div[@class='position-relative']/following-sibling::p)[2]")));
			clciElement.click();

		}

		@When("select the TP company name")
		public void select_the_tp_company_name() {
			WebDriverWait wat = new WebDriverWait(driver, 10);
			WebElement ele = wat
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select/option[@value='Flocki lab']")));
			ele.click();
			driver.findElement(By.xpath("//button[text()=' Save ']")).click();
			WebElement ls = wat.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("(//div[@class='position-relative']/following-sibling::p)[3]")));
			ls.click();
		}

		@When("Enter the loss details section details")
	

	
		    public String enterLossDetails() {
		        WebDriverWait actionsWait = new WebDriverWait(driver, 20);
		        WebElement actions = actionsWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//span[@class='mat-mdc-button-touch-target'])[1]")));
		        actions.click();

		        WebDriverWait detailsWait = new WebDriverWait(driver, 20);
		        WebElement details = detailsWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()=' 6 ']")));
		        details.click();
		        
		        return generateRandomDetails();
		    }

		    private String generateRandomDetails() {
		        String s = RandomStringUtils.randomAlphabetic(13);
		        String n = RandomStringUtils.randomNumeric(3);
		        return s + n;
		    }
		


		@When("Click save button")
		public void click_save_button() throws InterruptedException {

			Thread.sleep(10000);
			WebElement buttonElement1 = driver.findElement(By.cssSelector("button.btn.black-btn-bg"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", buttonElement1);

		}

		@When("click the police report section")
		public void click_the_police_report_section() {
			WebDriverWait ew = new WebDriverWait(driver, 10);
			ew.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("(//div[@class='position-relative']/following-sibling::p)[4]")))
					.click();

			WebDriverWait e = new WebDriverWait(driver, 10);
			WebElement c = e.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='file']")));
			// c.click();
			c.sendKeys("C:\\Users\\cbt\\Desktop\\604193079.pdf");

		}

		@When("User click the savee button")
		public void user_click_the_savee_button() {
			WebDriverWait b = new WebDriverWait(driver, 20);
			WebElement unt = b
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[normalize-space()='Save']")));
			unt.click();

		}

		@When("User click the garage details")
		public void user_click_the_garage_details() throws InterruptedException {
			WebDriverWait b = new WebDriverWait(driver, 20);
			WebElement ele = b
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p/span[text()=' Garage Details']")));
			ele.click();
			WebDriverWait b1 = new WebDriverWait(driver, 20);
			WebElement ele1 = b1.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//select/option[@value='Auto Diagnostics and Repair Center']")));
			ele1.click();

			Thread.sleep(1000);
			WebElement buttonElement1 = driver.findElement(By.cssSelector("button.btn.black-btn-bg"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", buttonElement1);
			// driver.findElement(By.xpath("//select/option[@value='Auto Diagnostics and
			// Repair Center']")).click();
		}

		@When("User click the survey details")
		public void user_click_the_survey_details() {
			driver.findElement(By.xpath("//p/span[text()=' Survey Details']")).click();
			WebDriverWait mat = new WebDriverWait(driver, 20);
			WebElement clickAction = mat.until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("(//span[@class='mat-mdc-button-touch-target'])[1]")));
			clickAction.click();
			driver.findElement(By.xpath("//span[text()=' 7 ']")).click();
			// clickAction=
			// mat.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()='
			// Save ']")));
			// clickAction.click();

		}

		@When("User click to save the survey details")
		public void user_click_to_save_the_survey_details() {
			WebElement buttonElement1 = driver.findElement(By.cssSelector("button.btn.black-btn-bg"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", buttonElement1);
			driver.findElement(By.xpath("//span[text()='Submit']")).click();
			
		}
		
		public void beforeclass(Scenario scnea) {
			 boolean failed = scnea.isFailed();
			
			 System.out.println("failed status "+failed);
			 if(failed) {
				 
				 byte[] screenshotAs = driver.getScreenshotAs(OutputType.BYTES);
				 scnea.attach(screenshotAs, "img/png", "screenshot failed");
				 
				 
			 }
		}

	}



