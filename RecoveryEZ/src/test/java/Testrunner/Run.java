package Testrunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = {"C:\\Users\\cbt\\eclipse-workspace\\RecoveryEZ\\Feature\\Recover.feature"},
//		dryRun = !false,
		monochrome = true,
		glue = {"pageobject"},
		plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}

		
		
		)

public class Run  {

}
