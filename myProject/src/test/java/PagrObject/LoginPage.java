package PagrObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage {

  

   @FindBy(id = "userName")
   @CacheLookup
   WebElement txtUsername;

   @FindBy(id = "userPassword")
   @CacheLookup
   WebElement txtPassword;

   @FindBy(xpath = "//button[contains(text(),'Login')]")
   @CacheLookup
   WebElement btnLogin;
   WebElement receivablebtn;
   
    WebDriver driver;
    public LoginPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    }

    public void enterUsername(String username) {
 
        txtUsername.clear();
        txtUsername.sendKeys(username);
    }


    public void enterPassword(String password) {
     
    	txtPassword.clear();
        txtPassword.sendKeys(password);
    }

    public void clickLogin() {      
    	btnLogin.click();
        
      
    }
    public void recebutton() throws InterruptedException {
    	//WebDriverWait wait = new WebDriverWait(driver, 10);
         //WebElement receivablebtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[./img[@src='/assets/icons/list2.svg']]")));
    	Thread.sleep(1000);
    	receivablebtn.click();
    }
    
        public void reportloss() {
    	WebDriverWait wait1 = new WebDriverWait(driver, 10);   	
    	WebElement receivablebtn1 = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[normalize-space()='Report Loss']")));
 	     receivablebtn1.click();                                                                            

    	
    }
    
    public void insured() {
    	WebDriverWait wait2 = new WebDriverWait(driver, 10);
    	WebElement receivablebtn2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='assets/reportloss/stage-icons/Insured Details.svg']")));
    	receivablebtn2.click();
    	
    }
    public void regis() {
    	WebDriverWait wait3 = new WebDriverWait(driver, 10);
    	WebElement receivablebtn1 = wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@type='text'])[1]")));
    	receivablebtn.click();
    	receivablebtn1.sendKeys("REG8732883728732");
    }
    public void make() {
    	
    	WebElement dropdown = driver.findElement(By.xpath("//select[@class='form-select form-control ng-pristine ng-valid ng-touched']"));
    	Select dropdownSelect = new Select(dropdown);
    	dropdownSelect.selectByValue("ABI");
    }
}
