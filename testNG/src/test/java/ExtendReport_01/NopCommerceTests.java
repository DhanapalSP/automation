package ExtendReport_01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NopCommerceTests {
	
	WebDriver driver;
	
	@BeforeClass
	void setup() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		Thread.sleep(1000);
		
		driver.get("https://demo.nopcommerce.com/");
		driver.manage().window().maximize();
		}
	
	@AfterClass
	void tearDown() {
		driver.quit();
	}
	
	
	@Test(priority = 1)
	public void testlogo() {
		try {
			boolean displayed = driver.findElement(By.xpath("//img[@alt='nopCommerce demo store']")).isDisplayed();
			Assert.assertEquals(displayed, true);
		}
		catch(Exception e){
			Assert.assertTrue(false);
			
		}
	}
		@Test(priority = 2)
		public void testlogin() {
			try {
				driver.findElement(By.linkText("Log in")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("abc123@gmail.com");
				driver.findElement(By.xpath("//input[@id='Password']")).sendKeys("test123");
				driver.findElement(By.xpath("//button[normalize-space()='Log in']")).click();
				Thread.sleep(3000);
				
				boolean displayed = driver.findElement(By.linkText("My account")).isDisplayed();
				Assert.assertEquals(displayed, true);
				
			}
			catch(Exception e) {
				Assert.fail();
			}}
			@Test(priority = 3,dependsOnMethods = {"testlogin"})
			 void logout() throws InterruptedException {
				driver.findElement(By.linkText("Log out")).click();
				Thread.sleep(3000);
				boolean displayed = driver.findElement(By.linkText("Register")).isDisplayed();
				Assert.assertEquals(displayed, true);
			}
			
			
			
		}
	
	
	
	
	

	


