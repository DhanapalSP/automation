package ExtendReport_01;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;


public class ExtendReportManager implements ITestListener{
	
	public ExtentSparkReporter sparreport;
	public ExtentReports  report;
	public ExtentTest test;
	
	
	public void onTest(ITestContext contest) {
		
		
		sparreport = new ExtentSparkReporter(System.getProperty("C:\\Users\\cbt\\eclipse-workspace\\testNG\\reports\\myReport.html"));
		sparreport.config().setDocumentTitle("Automation report");
		sparreport.config().setReportName("Functional Testing");
		sparreport.config().setTheme(Theme.STANDARD);
			
		report=new ExtentReports();
		report.attachReporter(sparreport);
		
		report.setSystemInfo("Computer Name","localhst");
		report.setSystemInfo("Environment", "QA");
		report.setSystemInfo("Testername", "Dhanapal");
		report.setSystemInfo("os", "windows11");
		report.setSystemInfo("Browsername", "chrome");
		
		}
	
	public void onTestSuccess(ITestResult result) {
		test = report.createTest(result.getName());
		test.log(Status.PASS, "Test case passed: "+result.getName());
	}
	public void onTestFaiure(ITestResult res) {
		
		test = report.createTest(res.getName());
		test.log(Status.FAIL, "Testcasefailed "+ res.getName());
		test.log(Status.FAIL, "Testcasepassed "+res.getThrowable());	
	}
	public void ontestskip(ITestResult skip) {
		test = report.createTest(skip.getName());
		test.log(Status.SKIP, "Testcaseskipped "+skip.getName());
		
	}
	public void onFinish(ITestContext context) {
        if (report != null) {
            report.flush();
        } else {
            // Handle the case where report is null
            System.out.println("ExtentReports object is null. Cannot flush.");
        }
	}}


