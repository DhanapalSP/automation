package StepDef;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageFac.pageclass;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import junit.framework.Assert;

public class Step {

	private WebDriver driver;
	private pageclass page;
	pageclass page1 = new pageclass(driver);

	public Step() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		page = new pageclass(driver); // Initialize page object
	}

	@Given("the user launch the broser {string}")
	public void the_user_launch_the_broser(String url) {
		driver.get(url); // Use driver to navigate to URL
	}

	@When("user enteres as username {string} and password as {string}")
	public void user_enteres_as_username_and_password_as(String uname, String pwd) {
		page.setUsername(uname);
		page.setPass(pwd);
	}

	@When("user clicks on login button")
	public void user_clicks_on_login_button() {
		page.clicklogin();

	}

	@When("check whether dashboard page is displayed")
	public void check_whether_dashboard_page_is_displayed() {

		boolean displayed = driver.findElement(By.id("prof_name")).isDisplayed();
		Assert.assertEquals(displayed, true);

	}

	@When("click user click the create user button")
	public void click_user_click_the_create_user_button() throws InterruptedException {

		page.waitForCreateButton();

	}

	@When("click the leave request button")
	public void click_the_leave_request_button() throws InterruptedException {
		Thread.sleep(1000);
		page.leavereqes();

	}

	@When("Click to apply the leave")
	public void click_to_apply_the_leave() {

		page.applyleave();
	}

	@When("click to select the dropdown list and select the list")
	public void click_to_select_the_dropdown_list_and_select_the_list() throws InterruptedException {

		Thread.sleep(1000);
		page.selectdrop();
		

	}

	@When("enter the reason for the leave request {string}")
	public void enter_the_reason_for_the_leave_request(String des) {
	   page.enterReasone(des);
	}
	
	@When("click to select the fromdate")
	public void click_to_select_the_and_todate() throws InterruptedException {

		
		  String month = "Jul"; // Set the desired month
		  Thread.sleep(500);
	        String year = "2024"; // Set the desired year
	        page.enterFromAndTo(month, year);
	    
	}
	@When("click to selet the Todate")
	public void click_to_selet_the_todate() throws InterruptedException {
		  String month = "Jul"; // Set the desired month
	        String year = "2024"; // Set the desired year
	        Thread.sleep(500);
	        page.entertodate(month, year);
	    
	}
	@When("click to Cancel button")
	public void click_to_cancel_button() {
	   page.cancelbutton();
	}


	


}
