package page;
	import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;




	public class TC_001_AccountRegistrationTest{
		
		WebDriver driver;
		@BeforeClass
		public void setup()
		
		
		{
			
			driver=new ChromeDriver();
			driver.manage().deleteAllCookies();
		
			
			driver.get("http://localhost/opencart/upload/index.php");
			driver.manage().window().maximize();
		}
		
		@AfterClass
		public void tearDown()
		{
			driver.close();
		}
		
		@Test
		public void verify_account_registration()
		{
//			HomePage hp=new HomePage(driver);
//			hp.clickMyAccount();
//			hp.clickRegister();
			
			test regpage = new test(driver);
			
			regpage.setfirstname(randomeString());
			regpage.setLastname(randomeString().toUpperCase());
			regpage.setemail(randomeString()+"@gmail.com");// randomly generated the email
			regpage.setTelephone(randomeNumber());
			
			String password=randomAlphaNumeric();
			
			regpage.setPassword(password);
			regpage.setConfirmPassword(password);
			
			regpage.setPrivacyPolicy();
			regpage.clickContinue();
			
			String confmsg=regpage.getConfirmationMsg();
			Assert.assertEquals(confmsg, "Your Account Has Been Created!");
			
		}

		public String randomeString()
		{
			String generatedString=RandomStringUtils.randomAlphabetic(5);
			return generatedString;
		}
		
		public String randomeNumber()
		{
			String generatedString=RandomStringUtils.randomNumeric(10);
			return generatedString;
		}
		
		public String randomAlphaNumeric()
		{
			String str=RandomStringUtils.randomAlphabetic(3);
			String num=RandomStringUtils.randomNumeric(3);
			
			return (str+"@"+num);
		
		
		}
		
	}











