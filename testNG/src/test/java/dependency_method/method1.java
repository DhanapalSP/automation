package dependency_method;



import org.testng.Assert;
import org.testng.annotations.Test;

public class method1 {
	
	
	
	
	@Test(priority = 0)
	void login() {
		Assert.assertTrue(true);		
	}
	@Test(priority = 1,dependsOnMethods = {"login"})
	void openapp() {
		Assert.assertTrue(true);	
		
	}
	@Test(priority = 2,dependsOnMethods = {"openapp"})
	void search () {
		Assert.assertTrue(true);	
		
	}
	@Test(priority = 3,dependsOnMethods = {"search"})
	void advanceserach() {
		Assert.assertTrue(true);	
		
	}
	@Test(priority = 4,dependsOnMethods = {"advanceserach"})
	void entervalur() {
		Assert.assertTrue(true);	
		
	}
	@Test(priority = 5,dependsOnMethods = {"entervalur"})
	void logout() {
		Assert.assertTrue(true);	
	}

}
