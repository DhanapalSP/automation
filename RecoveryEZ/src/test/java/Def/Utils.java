package Def;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Utils {

	
	 public static void retryClick(WebDriver driver, WebElement element) {
	       // WebDriverWait wait = new WebDriverWait(driver, 10);
	        int attempts = 0;
	        while (attempts < 2) {
	            try {
	                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	                break;
	            } catch (StaleElementReferenceException e) {
	                // Element is stale, retry clicking
	                element = driver.findElement(By.cssSelector("button.btn.black-btn-bg"));
	            }
	            attempts++;
	        }
	    }
}
