package pageobjectmodel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class page {

	//Constructor
	
	public WebDriver driver;
	
	page(WebDriver driver)
	{
		this.driver=driver;
	}
	
	//locator
	
	By img_log_loc = By.xpath("//img[@alt='company-branding']");
	By txt_username = By.name("username");
	By txt_pwd = By.cssSelector("input[placeholder='Password'");
	By btn_login=By.xpath("//button[normalize-space()='Login']");
	
	//Locator
	
	public void setUsername(String username) {
		driver.findElement(txt_username).sendKeys(username);
	}
	public void setPassword(String pwd) {
		driver.findElement(txt_pwd).sendKeys(pwd);
	}
	public void clickbtn() {
		driver.findElement(btn_login).click();
	}
	public boolean logoPresence() {
		boolean status = driver.findElement(img_log_loc).isDisplayed();
		return status;
	}
	
	
	
	
	
	
	
	
	
	
					
}
