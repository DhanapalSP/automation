package TestRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;



@RunWith(Cucumber.class)
@CucumberOptions(
	
		features = ".//myProject/Feature/new.feature",
		glue = "stefDefinition",
		dryRun =true,
		monochrome = true,
		plugin = {"pretty","html:test-output"}	
		)
public class testRun {

}
