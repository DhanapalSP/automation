package grouping;

import org.testng.annotations.Test;

public class grouping {
	
	
	@Test(groups = {"sanity"})
	void loginemail() {
		System.out.println("login by mail");
		
	}
	@Test
	void loginfacebook() {
		System.out.println("login by fb");
		
	}
	@Test
	void logininsta() {
		System.out.println("login by insta");
	}
	@Test(priority = 1)
	void verifylogin(){
		System.out.println("verify by login");
	}
	@Test
	void verifyfb() {
		System.out.println("verify by fb");
	}
	@Test
	void verifyinsta() {
		System.out.println("verify by inta");
	}

}
